<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EmbedWord
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EmbedWord))
Me.WordPicture = New System.Windows.Forms.PictureBox
Me.cmdEmbedWord = New System.Windows.Forms.Button
CType(Me.WordPicture, System.ComponentModel.ISupportInitialize).BeginInit()
Me.SuspendLayout()
'
'WordPicture
'
Me.WordPicture.Location = New System.Drawing.Point(3, 27)
Me.WordPicture.Name = "WordPicture"
Me.WordPicture.Size = New System.Drawing.Size(645, 367)
Me.WordPicture.TabIndex = 0
Me.WordPicture.TabStop = False
'
'cmdEmbedWord
'
Me.cmdEmbedWord.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.cmdEmbedWord.Location = New System.Drawing.Point(3, 400)
Me.cmdEmbedWord.Name = "cmdEmbedWord"
Me.cmdEmbedWord.Size = New System.Drawing.Size(115, 42)
Me.cmdEmbedWord.TabIndex = 1
Me.cmdEmbedWord.Text = "Embed Word"
Me.cmdEmbedWord.UseVisualStyleBackColor = True
'
'EmbedWord
'
Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
Me.ClientSize = New System.Drawing.Size(652, 444)
Me.Controls.Add(Me.cmdEmbedWord)
Me.Controls.Add(Me.WordPicture)
Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
Me.Margin = New System.Windows.Forms.Padding(4)
Me.Name = "EmbedWord"
Me.Text = " Embed Word into Form"
CType(Me.WordPicture, System.ComponentModel.ISupportInitialize).EndInit()
Me.ResumeLayout(False)

End Sub
    Friend WithEvents WordPicture As System.Windows.Forms.PictureBox
    Friend WithEvents cmdEmbedWord As System.Windows.Forms.Button

End Class
