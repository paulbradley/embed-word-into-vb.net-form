' See LICENSE file
' Copyright (c) 213 Paul Bradley
' (https://paulbradley.org)

Public Class EmbedWord

    Public Const HWND_TOPMOST = -1

    ' Retrieves a handle to the top-level window whose class name and window
    ' name match the specified strings. This function does not search
    ' child windows. This function does not perform a case-sensitive search.
    Public Declare Function FindWindow Lib "user32" Alias "FindWindowA" _
        (ByVal lpClassName  As String, _
         ByVal lpWindowName As String) As Integer

    ' Changes the parent window of the specified child window.
    Public Declare Function SetParent Lib "user32" _
        (ByVal hWndChild     As Integer, _
         ByVal hWndNewParent As Integer) As Long

    ' Changes the size, position, and Z order of a child, pop-up, or top-level window.
    ' These windows are ordered according to their appearance on the screen.
    ' The topmost window receives the highest rank and is the first window in the Z order.
    Public Declare Function SetWindowPos Lib "user32" _
        (ByVal hWnd            As Integer, _
         ByVal hWndInsertAfter As Integer, _
         ByVal x  As Integer, _
         ByVal y  As Integer, _
         ByVal cx As Integer, _
         ByVal cy As Integer, _
         ByVal wFlags As Integer) As Integer

    Private WithEvents ObjWord As Word.Application
    Private WordWND As Integer

    Private Sub cmdEmbedWord_Click( ByVal sender As System.Object,  ByVal e As System.EventArgs) Handles cmdEmbedWord.Click
        Try
            cmdEmbedWord.Enabled = False
            Me.Cursor = Cursors.AppStarting

            ' Load Word and make invisible
            ObjWord = New Word.ApplicationClass
            ObjWord.Visible = False

            ' Set a custom window title to make it
            ' easier to find the window handle
            ObjWord.Caption = "ALMA-ENHANCED-WORD"
            WordWND = FindWindow(vbNullString, "ALMA-ENHANCED-WORD")

            ' Make Word a child window of the picture control
            SetParent(WordWND, Me.WordPicture.Handle.ToInt32())

            ObjWord.WindowState = Word.WdWindowState.wdWindowStateNormal

            ' Positon Word to the co ordinates of the picture control
            SetWindowPos(WordWND, HWND_TOPMOST, _
                0, 0, Me.WordPicture.Width, Me.WordPicture.Height, Nothing)

            ' Add a document and make Word visible
            ObjWord.Documents.Add()
            ObjWord.Visible = True

            Me.Cursor = Cursors.Arrow

        Catch Ex as Exception
            MsgBox(Ex.ToString, MsgBoxStyle.OkOnly Or MsgBoxStyle.SystemModal, "Exception")
        End Try
    End Sub

    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        ObjWord.Quit(False)
    End Sub

End Class